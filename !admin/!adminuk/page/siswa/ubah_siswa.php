<?php 
include "../koneksi.php";
$id_siswa=$_GET['id_siswa'];
$query=$koneksi->query("SELECT * FROM siswa WHERE id_siswa=$id_siswa");
$data=$query->fetch_assoc();
?>

<p class="title-content"><b>Change Data Student</b></p>
<div id="hor-line"></div>

<form class="form-horizontal" method="POST" action="page/siswa/proses.php?aksi=ubah_siswa" enctype="multipart/form-data"> 
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label>Student Name * :</label>
				<input type="text" name="nama" value="<?php echo $data['nama_siswa']; ?>" class="form-control" autocomplete="off" required>
				<input type="hidden" name="id_siswa" value="<?php echo $data['id_siswa']; ?>" required>
			</div>
			<div class="form-group">
				<label>NIS * :</label>
				<input type="text" name="nis" value="<?php echo $data['nis']; ?>" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Address</label>
				<textarea name="alamat" class="form-control"><?php echo $data['alamat']; ?></textarea>
			</div>
		</div>
	</div><br>
	<button type="submit" class="btn btn-success">Change</button>
	<button type="button" class="btn btn-danger"><a href="?page=siswa">Back</a></button>
</form>