<p class="title-content"><b>Add Data Student</b></p>
<div id="hor-line"></div>

<form class="form-horizontal" method="POST" action="page/siswa/proses.php?aksi=tambah_siswa" enctype="multipart/form-data"> 
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label>Student Name * :</label>
				<input type="text" name="nama" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>NIS * :</label>
				<input type="text" name="nis" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Address</label>
				<textarea name="alamat" class="form-control"></textarea>
			</div>
		</div>
	</div><br>
	<button type="submit" class="btn btn-success">Add</button>
	<button type="button" class="btn btn-danger"><a href="?page=siswa">Back</a></button>
</form>