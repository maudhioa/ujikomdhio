<?php 
include "database.php";
$tampil = new database();
session_start();

$id=$_GET['id_peminjam'];
$si_peminjam=$tampil->tampil_sipeminjam($id);
?>
<p class="title-content"><b>Returning item/inventories</b></p>
<div id="hor-line"></div>
<p>Dipinjam oleh : <?php echo $si_peminjam['nama_peminjam']; ?></p>
<p>Level : <?php echo $si_peminjam['level']; ?></p>
<a class="btn btn-dark" href="?page=pengembalian">Back</a>
<nav class="nav nav-tabs" id="myTab" role="tablist" style="margin-top: 20px; font-size: 16px; margin-bottom: 15px;">
  <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Return</a>
  <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Return report</a>
</nav>


<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
  	<div class="table-responsive">
			<table class="table table-bordered table-hover" id="example">
				<thead>
					<tr>
						<th>No</th>
						<th>Code</th>
						<th>Item</th>
						<th>Total </th>
						<th>Borrow time</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no=1;
					$a = $tampil->tampil_pengembalian2("TELAH DIKEMBALIKAN",$id); 
					foreach ($a as $data) {
						?>
						<tr>
							<td><?php echo $no ?></td>	
							<td><?php echo $data['kode_peminjaman'] ?></td>	
							<td><?php echo $data['nama'] ?></td>	
							<td><b><?php echo $data['jumlah_pinjam'] ?></b> Buah/Unit</td>	
							<td><?php echo $data['tanggal_pinjam'] ?></td>	
							<td><?php echo $data['status_peminjaman'] ?></td>	
							<td>
								<?php echo "
								<a class='btn btn-danger' style='color:white' href='page/pengembalian/proses.php?aksi=kembalikan&kode_peminjaman=$data[kode_peminjaman]&id_peminjam=$id'>RETURN NOW</a> " 
								?>
							</td>	
						</tr>
						<?php
						$no++;
					}
					?>
				</tbody>
			</table>
		</div>
  </div>

  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
  	<div class="table-responsive">
			<table class="table table-bordered table-hover" id="example">
				<thead>
					<tr>
						<th>No</th>
						<th>Code</th>
						<th>Item</th>
						<th>Total</th>
						<th>Borrow time</th>
						<th>Return time</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no=1;
					$a = $tampil->tampil_pengembalian3("TELAH DIKEMBALIKAN",$id); 
					foreach ($a as $data) {
						?>
						<tr>
							<td><?php echo $no ?></td>	
							<td><?php echo $data['kode_peminjaman'] ?></td>	
							<td><?php echo $data['nama'] ?></td>	
							<td><b><?php echo $data['jumlah_pinjam'] ?> </b> Buah/Unit</td>	
							<td><?php echo $data['tanggal_pinjam'] ?></td>	
							<td><?php echo $data['tanggal_kembali'] ?></td>	
							<td><?php echo $data['status_peminjaman'] ?></td>		
							<td>
								<!--<?php echo "
								<a data-toggle='modal' data-target='#myModal$data[id_petugas]'
								class='btn btn-info'><b>i</b></a> "; ?>-->
								<a onclick="return confirm('Apakah anda yakin???')" 
								href="page/pengembalian/proses.php?aksi=hapus2&kode_peminjaman=<?php echo $data['kode_peminjaman']?>&id_peminjam=<?php echo $id;?>" class="btn btn-danger">DELETE</a>
							</td>	
						</tr>
						<?php
						$no++;
					}
					?>
				</tbody>
			</table>
		</div>
  </div>
</div>

