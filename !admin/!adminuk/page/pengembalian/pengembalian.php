<?php 
include "database.php";
$tampil = new database();
?>
<p class="title-content"><b>Return Item</b></p>
<div id="hor-line"></div>


<nav class="nav nav-tabs" id="myTab" role="tablist" style="margin-top: 20px; font-size: 16px; margin-bottom: 15px;">
  <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Return Now</a>
  <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Full List</a>
</nav>


<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
  	<a href="?page=pengembalian&kej=lengkap" class="btn btn-info" style="margin-bottom: 10px;">Full</a>
  	<div class="table-responsive">
			<table class="table table-bordered table-hover" id="example">
				<thead>
					<tr>
						<th>No</th>
						<th>Name</th>
						<th>Level</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no=1;
					$a = $tampil->tampil_pengembalian("TELAH DIKEMBALIKAN"); 
					foreach ($a as $data) {
						?>
						<tr>
							<td><?php echo $no ?></td>	
							<td><?php echo $data['nama_peminjam'] ?></td>	
							<td><?php echo $data['level'] ?></td>	
							<td>
								<?php echo "
								<a class='btn btn-danger' style='color:white' href='?page=pengembalian&kej=kembalikan&id_peminjam=$data[id_peminjam]'>LOOK</a> " 
								?>
							</td>	
						</tr>
						<?php
						$no++;
					}
					?>
				</tbody>
			</table>
		</div>
  </div>

  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
  	<p align="right" style="margin-bottom: 10px;">Make Report :  <a href="page/pengembalian/lap_excel.php" class="btn btn-dark">Ex</a>
	<a href="page/pengembalian/lap_pdf.php" class="btn btn-dark">Pdf / Print</a></p>
  	<div class="table-responsive">
			<table class="table table-bordered table-hover" id="example">
				<thead>
					<tr>
						<th>No</th>
						<th>Code</th>
						<th>Name</th>
						<th>Level</th>
						<th>Item</th>
						<th>Total</th>
						<th>Borrow time</th>
						<th>Return time</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no=1;
					$a = $tampil->pengembalian_lengkap("TELAH DIKEMBALIKAN"); 
					foreach ($a as $data) {
						?>
						<tr>
							<td><?php echo $no ?></td>	
							<td><?php echo $data['kode_peminjaman'] ?></td>	
							<td><?php echo $data['nama_peminjam'] ?></td>	
							<td><?php echo $data['level'] ?></td>	
							<td><?php echo $data['nama'] ?></td>	
							<td><b><?php echo $data['jumlah_pinjam'] ?></b></td>	
							<td><?php echo $data['tanggal_pinjam'] ?></td>	
							<td><?php echo $data['tanggal_kembali'] ?></td>	
							<td><?php echo $data['status_peminjaman'] ?></td>	
							<td>
								<?php echo "
								<a class='btn btn-danger' style='color:white' href='page/pengembalian/proses.php?aksi=hapus&kode_peminjaman=$data[kode_peminjaman]'>DELETE</a> " 
								?>
							</td>	
						</tr>
						<?php
						$no++;
					}
					?>
				</tbody>
			</table>
		</div>
  </div>
</div>

