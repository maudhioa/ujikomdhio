<?php 
session_start();
include "../koneksi.php";
$id_inventaris=$_GET['id_inventaris'];
$query=$koneksi->query("SELECT * FROM inventaris i JOIN ruang r ON i.id_ruang=r.id_ruang WHERE id_inventaris=$id_inventaris");
$data=$query->fetch_assoc();
?>

<p class="title-content"><b>Tambah Data Barang Rusak</b></p>
<div id="hor-line"></div>

<form class="form-horizontal" method="POST" action="page/barang/proses.php?aksi=rusak" enctype="multipart/form-data"> 
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label>Nama Barang * :</label>
				<input type="text" value="<?php echo $data['nama'] ?>" name="nama_barang" class="form-control" autocomplete="off" required readonly>
				<input type="hidden" value="<?php echo $data['id_inventaris'] ?>" name="id_inventaris">
				<input type="hidden" value="<?php echo $_SESSION['id_pengguna'] ?>" name="id_petugas">
			</div>
			<div class="form-group">
				<label>Ruangan * :</label>
				<input type="text" value="<?php echo $data['nama_ruang'] ?>" name="ruangan" class="form-control" autocomplete="off" required readonly>
			</div>
			<div class="form-group">
				<label>Jumlah Rusak * : </label>
				<input type="number" value="1" name="jumlah" class="form-control" autocomplete="off" required>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label>Pelapor * :</label>
				<input type="text" value="<?php echo $_SESSION['nama_pengguna'] ?>" name="pelapor" class="form-control" autocomplete="off" required readonly>
			</div>
				<label>Keterangan / Alasan :</label>
				<textarea class="form-control" name="keterangan" required></textarea>
		</div>
	<button type="submit" class="btn btn-success">Tambah</button>
	<button type="button" class="btn btn-danger"><a href="?page=barang">kembali</a></button>
		</div>
</form>