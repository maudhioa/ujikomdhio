<?php 
	session_start();
?>
<p class="title-content"><b>Add Data Inventories/Item</b></p>
<div id="hor-line"></div>

<form class="form-horizontal" method="POST" action="page/barang/proses.php?aksi=tambah_barang" enctype="multipart/form-data"> 
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label>Item Name * :</label>
				<input type="text" name="nama_barang" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Code * :</label>
				<input type="text" name="kode" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Total * :</label>
				<input type="number" name="jumlah" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Condition * :</label>
				<select name="kondisi" class="form-control">
					<option>Buruk</option>
					<option selected>Baik</option>
					<option>Sangat Baik</option>
				</select>
				<input type="hidden" name="id_petugas" value="<?php echo $_SESSION['id_pengguna']; ?>">
			</div>
	
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label>Rooms/Location * :</label>
				<select class="form-control" name="ruang" required>
					<?php
					include "koneksi.php";
					$query=$koneksi->query("SELECT * FROM ruang");
					while($sql=$query->fetch_assoc()) {
					?>
						<option value=<?php echo $sql["id_ruang"] ?>> <?php echo $sql["nama_ruang"] ?></option>                 
					<?php ;
					}               
					?>
				</select>
			</div>
			<div class="form-group">
				<label>Type * :</label>
				<select class="form-control" name="jenis" required>
					<?php
					include "koneksi.php";
					$query=$koneksi->query("SELECT * FROM jenis");
					while($sql=$query->fetch_assoc()) {
					?>
						<option value=<?php echo $sql["id_jenis"] ?>> <?php echo $sql["nama_jenis"] ?></option>                 
					<?php ;
					}               
					?>
				</select>
			</div>
				<label>Information :</label>
				<textarea class="form-control" name="keterangan"></textarea>
				<label>Foto/Image * :</label>
				<input class="form-control" type="file" name="foto">
		</div>
	</div>
	<button type="submit" class="btn btn-success">Add</button>
	<button type="button" class="btn btn-danger"><a href="?page=barang">Back</a></button>
</form>