var name = 'László Horváth';

var getMonogram = function getMonogram(name) {
  var words = name.toUpperCase().split(' ');
  var result = '';

  words.forEach(function (value) {
    result += value[0];
  });

  return result;
};

var monogram = getMonogram(name);

$('.profile__monogram').text(monogram);
$('.profile__name').text(name);