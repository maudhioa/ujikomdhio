<?php 
include "../koneksi.php";
$id_guru=$_GET['id_guru'];
$query=$koneksi->query("SELECT * FROM guru WHERE id_guru=$id_guru");
$data=$query->fetch_assoc();
?>

<p class="title-content"><b>Ubah Data Guru</b></p>
<div id="hor-line"></div>

<form class="form-horizontal" method="POST" action="page/guru/proses.php?aksi=ubah_guru" enctype="multipart/form-data"> 
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label>Nama Guru * :</label>
				<input type="text" name="nama" value="<?php echo $data['nama_guru']; ?>" class="form-control" autocomplete="off" required>
				<input type="hidden" name="id_guru" value="<?php echo $data['id_guru']; ?>" required>
			</div>
			<div class="form-group">
				<label>NIP * :</label>
				<input type="text" name="nip" value="<?php echo $data['nip']; ?>" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Email * :</label>
				<input type="text" name="email" value="<?php echo $data['email']; ?>" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Alamat</label>
				<textarea name="alamat" class="form-control"><?php echo $data['alamat']; ?></textarea>
			</div>
		</div>
	</div><br>
	<button type="submit" class="btn btn-success">Ubah</button>
	<button type="button" class="btn btn-danger"><a href="?page=siswa">kembali</a></button>
</form>