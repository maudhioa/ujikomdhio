<?php 
	include "database.php";
	$z = new database();
	$a = $z->tampil_jenis();
?>

<p class="title-content"><b>Tabel Data Jenis</b></p>
<div id="hor-line"></div>

<div class="row">
	<div class="col-md-9">
<a href="?page=jenis&kej=tambah" class="btn btn-success" style="margin-bottom: 15px;"><b>+</b> Tambah Jenis</a> 
</div>
<!--<p align="right"> Buat Laporan :  <a href="page/barang/lap_excel.php" class="btn btn-dark">Ex</a>
<a href="page/barang/lap_pdf.php" class="btn btn-dark">Pdf</a></p>-->
</div>

<?php 
include "../koneksi.php";
$modal=$koneksi->query("SELECT * FROM jenis");
while ($sq = $modal->fetch_assoc()) 
{
	?>
	<div class="modal fade" id="detail<?=$sq['id_jenis'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="max-width: 700px;">
			<div class="modal-content">
				<div class="modal-header">
					<h3><b>Data Lengkap Operator</b></h3><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<thead></thead>
							<tbody>
								<form action="page/jenis/proses.php?aksi=ubah" method="POST">
								<?php
								$dataa=$koneksi->query("SELECT * FROM jenis WHERE id_jenis=$sq[id_jenis]" );
								$no=1;
								while ($ra=$dataa->fetch_assoc())
								{	
									echo"<tr>";
									echo '<th>Nama jenis</th>';
									echo "<td><input type='text' name='nama' class='form-control' value='$ra[nama_jenis]'>
									<input type='hidden' name='id_jenis' class='form-control' value='$ra[id_jenis]'></td>";
									echo"</tr>";

									echo"<tr>";
									echo '<th>Kode jenis</th>';
									echo "<td><input type='text' name='kode' class='form-control' value='$ra[kode_jenis]'></td>";
									echo"</tr>";

									echo"<tr>";
									echo '<th>Keterangan</th>';
									echo "<td><input type='text' name='keterangan' class='form-control' value='$ra[keterangan]'></td>";
									echo"</tr>";

									echo"<tr>";
									echo "<th><button type='submit' class='btn btn-info' >Ubah</button</th>";
									echo"</tr>";
									$no++;
								}
								?>
								</form>
							</tbody>
						</table>
					</div>		
				</div>
			</div>
		</div>
	</div>
<?php } ?>





<div class="table-responsive">
	<table class="table table-bordered table-hover" id="example">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Jenis</th>
				<th>Kode</th>
				<th>keterangan</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
		<?php
		$no=1; 
		foreach ($a as $data) {
		?>
		<tr>
			<td><?php echo $no ?></td>	
			<td><?php echo $data['nama_jenis'] ?></td>	
			<td><?php echo $data['kode_jenis'] ?></td>	
			<td><?php echo $data['keterangan'] ?></td>		
			<td>
				<?php echo "<a data-toggle='modal' data-target='#detail$data[id_jenis]' href='' 
				class='btn btn-info'><img src='../img/change.png' width='16px'></a>"; ?>
				<a onclick="return confirm('Apakah anda yakin???')" 
				href="page/jenis/proses.php?aksi=hapus&id=<?php echo $data['id_jenis'] ?>" class="btn btn-danger"><img src='../img/delete.png' width='16px'></a>
			</td>	
		</tr>
		<?php
		$no++;
		}
		 ?>
		</tbody>
	</table>
</div>


<?php 
//include "modal.php";
?>
