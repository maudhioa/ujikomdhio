<?php 
session_start();
include "../koneksi.php";
$id_inventaris=$_GET['id_inventaris'];
$query=$koneksi->query("SELECT * FROM inventaris WHERE id_inventaris=$id_inventaris");
$data=$query->fetch_assoc();
?>

<p class="title-content"><b>Tambah Data Barang / <INS></INS>ventaris</b></p>
<div id="hor-line"></div>

<form class="form-horizontal" method="POST" action="page/barang/proses.php?aksi=tambah2" enctype="multipart/form-data"> 
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label>Nama Barang * :</label>
				<input type="text" value="<?php echo $data['nama'] ?>" name="nama_barang" class="form-control" autocomplete="off" required readonly>
				<input type="hidden" value="<?php echo $data['id_inventaris'] ?>" name="id_barang">
				<input type="hidden" value="<?php echo $_SESSION['id_pengguna'] ?>" name="id_petugas">
			</div>
			<div class="form-group">
				<label>Kode * :</label>
				<input type="text" value="<?php echo $data['kode_inventaris'] ?>" name="kode" class="form-control" autocomplete="off" required readonly>
			</div>
			<div class="form-group">
				<label>Jumlah * : </label>
				<input type="number"  name="jumlah" class="form-control" autocomplete="off" required>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label>Kondisi * :</label>
				<input type="text" value="<?php echo $data['kondisi'] ?>" name="kode" class="form-control" autocomplete="off" required readonly>
			</div>
				<label>Keterangan :</label>
				<textarea class="form-control" name="keterangan"><?php echo $data['keterangan']; ?></textarea>
		</div>
	<button type="submit" class="btn btn-success">Tambah</button>
	<button type="button" class="btn btn-danger"><a href="?page=barang">kembali</a></button>
		</div>
</form>