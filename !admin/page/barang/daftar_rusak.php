<?php 
	include "database.php";
	$z = new database();
	$a = $z->tampil_barang_rusak();
?>

<p class="title-content"><b>Tabel Data Barang / Inventaris Rusak</b></p>
<div id="hor-line"></div>

<div class="row">
	<div class="col-md-9">

<a href="?page=barang" class="btn btn-dark" style="margin-bottom: 15px;"><b></b> Kembali</a> 
</div>
</div>

<div class="table-responsive">
	<table class="table table-bordered table-hover" id="example">
		<thead>
			<tr>
				<th>No</th>
				<th>id Barang</th>
				<th>Nama</th>
				<th>Jumlah</th>
				<th>Ruangan</th>
				<th>Pelapor</th>
				<th>Waktu</th>
				<th>Keterangan</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
		<?php
		$no=1; 
		foreach ($a as $data) {
		?>
		<tr>
			<td><?php echo $no ?></td>	
			<td><?php echo $data['id_inventaris'] ?></td>	
			<td><?php echo $data['nama_barang'] ?></td>	
			<td><?php echo $data['jumlah'] ?></td>	
			<td><?php echo $data['ruangan'] ?></td>	
			<td><?php echo $data['pelapor'] ?></td>	
			<td><?php echo $data['waktu'] ?></td>	
			<td><?php echo $data['keterangan'] ?></td>	
			<td>
				<a onclick="return confirm('Apakah anda yakin???')" 
				href="page/barang/proses.php?aksi=kembalikan_rusak&id_rusak=<?php echo $data['id_rusak'] ?>&id_inventaris=<?php echo $data['id_inventaris'] ?>&jumlah=<?php echo $data['jumlah'] ?>" class="btn btn-success">KEMBALIKAN</a>
			</td>	
		</tr>
		<?php
		$no++;
		}
		 ?>
		</tbody>
	</table>
</div>


<?php 
//include "modal.php";
?>
