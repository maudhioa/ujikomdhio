<?php 
include "database.php";
$data = new database();
$tampil = $data->tampil_barang(); 
?>

<link rel="stylesheet" type="text/css" href="../../assets/css/style.css">

<input type="button" value="Buat PDF" onclick="window.print()" id="cetak" class="no-print"> 

<p align="center" id="title-laporan"><img src="../../img/bogor.png" width="80px" style="float: left; margin-left: 20px;"><img src="../../img/ciomas.png" width="90px" style="float: right;"><b>PEMERINTAH KABUPATEN BOGOR<br>DINAS PENDIDIKAN<br>SMK NEGERI 1 CIOMAS<br>BIDANG STUDI KEAHLIAN TEKNOLOGI DAN REKAYASA<BR>TEKNOLOGI INFORMASI DAN KOMUNIKASI</b></p>

<p align="center" id="title-laporan2">Jln. Raya Laladon Desa Laladon Kec Ciomas Kab Bogor Telp : (0251 7520933) Kode Pos. 16610</p><br>

<div style="height: 3px;width: 100%;background-color: #3a3a3a;margin-bottom: 10px;border-radius: 10px;"></div>
<p align="center" style="margin-top: 35px;">DATA BARANG</p>

<table border="1" width="100%" style="border-collapse: collapse;">
	<thead class="title-table">
		<tr style="height: 40px;">
			<th>No</th>
			<th>Nama</th>
			<th>Jumlah</th>
			<th>Kondisi</th>
			<th>Nama Jenis</th>
			<th>Nama ruang</th>
			<th>Tanggal Register</th>
			<th>Nama_petugas</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$no=1;
		foreach ($tampil as $a) {
		?>
		<tr style="height: 30px; text-align: center;">
			<td><?php echo $no; ?></td>
			<td><?php echo $a["nama"]; ?></td>
			<td><?php echo $a["jumlah"]; ?></td>
			<td><?php echo $a["kondisi"]; ?></td>
			<td><?php echo $a["nama_jenis"]; ?></td>
			<td><?php echo $a["nama_ruang"]; ?></td>
			<td><?php echo $a["tanggal_register"]; ?></td>
			<td><?php echo $a["nama_petugas"]; ?></td>
		</tr>
		<?php 	
		 $no++;
		 } 
		 ?>
	</tbody>
</table>
