<?php 
	include "database.php";
	$aksi = $_GET['aksi'];
	$db = new database();

	if($aksi=="hapus_siswa"){
 		$db->hapus_siswa($_GET['id']);
	 	if ($db) {
	 		?>
		 	<script type="text/javascript">
				alert("Data Berhasil dihapus");
				window.location.href="../../?page=siswa";
			</script>
	 	<?php
	 	}else{
	     	echo "HAPUS GAGAL";
	     }
 	}
 	if ($aksi=="tambah_siswa") {
 		$db->tambah_siswa($_POST['nama'],$_POST['nis'],$_POST['alamat'],$_POST['email']);
	 	if ($db) {
	 		?>
		 	<script type="text/javascript">
				alert("Data Berhasil ditambah");
				window.location.href="../../?page=siswa";
			</script>
	 	<?php
	 	}else{
	     	echo "INPUT GAGAL";
	     }
 	}
 	if ($aksi=="ubah_siswa") {
 		$db->ubah_siswa($_POST['id_siswa'],$_POST['nama'],$_POST['nis'],$_POST['alamat'],$_POST['email']);
	 	if ($db) {
	 		?>
		 	<script type="text/javascript">
				alert("Data Berhasil diubah");
				window.location.href="../../?page=siswa";
			</script>
	 	<?php
	 	}else{
	     	echo "INPUT GAGAL";
	     }
 	}

?>