<?php 
	include "database.php";
	$z = new database();
	$a = $z->tampil_peminjam();
?>

<p class="title-content"><b>Tabel Data Siswa</b></p>
<div id="hor-line"></div>

<div class="row">
	<div class="col-md-9">

<a class="btn btn-success" href="?page=siswa&kej=tambah" class="btn btn-success" style="margin-bottom: 15px;"><b>+</b> Tambah Data Siswa</a> 
</div>
<!--<p align="right" style="color: black;"> Buat Laporan :  <a href="" class="btn btn-dark">Ex</a>
<a href="" class="btn btn-dark">Pdf</a></p>-->
</div>

<?php 
include "../koneksi.php";
$modal=$koneksi->query("SELECT * FROM siswa");
while ($sq = $modal->fetch_assoc()) 
{
	?>
	<div class="modal fade" id="myModal<?=$sq['id_siswa'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h3><b>Data Lengkap Siswa</b></h3><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<thead></thead>
							<tbody>
								<?php
								$dataa =$koneksi->query("SELECT * FROM siswa 
									WHERE id_siswa='$sq[id_siswa]'");
								$no=1;
								while ($ra=$dataa->fetch_assoc())
								{														
									echo"<tr>";
									echo '<th>Nama</th>';
									echo "<td>$ra[nama_siswa]</td>";
									echo"</tr>";

									echo"<tr>";
									echo'<th>NIS</th>';
									echo "<td>$ra[nis]</td>";
									echo"</tr>";

									echo"<tr>";
									echo'<th>Alamat</th>';			
									echo "<td>$ra[alamat]</td>";
									echo"</tr>";
									
									echo"<tr>";
									echo'<th>Email</th>';			
									echo "<td>$ra[email]</td>";
									echo"</tr>";

									$no++;
								}
								?>
							</tbody>
						</table>
					</div>		
				</div>
			</div>
		</div>
	</div>
<?php } ?>

<div class="table-responsive">
	<table class="table table-bordered table-hover" id="example">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Siswa</th>
				<th>Nis</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
		<?php
		$no=1; 
		foreach ($a as $data) {
		?>
		<tr>
			<td><?php echo $no ?></td>	
			<td><?php echo $data['nama_siswa'] ?></td>	
			<td><?php echo $data['nis'] ?></td>	
			<td>
				<?php echo "
				<a data-toggle='modal' data-target='#myModal$data[id_siswa]'
				class='btn btn-info'><b><img src='../img/eye.png' width='16px'></b></a> "; ?>
				<a class='btn btn-dark' style='color:white' href='?page=siswa&kej=ubah&id_siswa=<?php echo $data[id_siswa] ?>'><img src='../img/change.png' width='16px'></a>
				<a onclick="return confirm('Apakah anda yakin???')" 
				href="page/siswa/proses.php?aksi=hapus_siswa&id=<?php echo $data['id_siswa'] ?>" class="btn btn-danger"><img src='../img/delete.png' width='16px'></a>
			</td>	
		</tr>
		<?php
		$no++;
		}
		 ?>
		</tbody>
	</table>
</div>


<?php 
include "modal.php";
?>
