<?php 
	session_start();
	include "koneksi.php";
	error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Maudhio Andre | UJIKOM | INVENTARIS SEKOLAH </title>
	<link rel="icon" href="../img/titlesmk.png">
  	<meta charset="utf-8">
  	<meta name="keywords" />
  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
  	<!-- CSS Bootstrap -->
  	<link rel="stylesheet" type="text/css" href="bt4/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="bt4/css/style.css">
  	<!-- JS Bootstrap -->
  	<script type="text/javascript" src="bt4/js/jquery.js"></script>
  	<script type="text/javascript" src="bt4/js/bootstrap.min.js"></script>
  	<script type="text/javascript" src="bt4/js/jquery-1.11.1.min.js"></script>
  	<!-- Data Table -->
  	<link rel="stylesheet" type="text/css" href="data_table/assets/css/jquery.dataTables.css">
  	<!-- CSS PRIBADI -->
  	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/login.css">

</head>
<body style="background:url(img/login2.jpg);">

<div class="cont">
  <div class="demo">
    <div class="login">
      <p align="center"><img src="img/titlesmk.png" style="width: 50%; padding-top: 18%; margin-right:10px; "></p>
      <div class="login__form" style="margin-top: -35px">
        <form action="" method="POST">
        <div class="login__row">
          <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
            <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
          </svg>
          <input type="text" class="login__input name" placeholder="Nama lengkap" 
          maxlength="20" autocomplete="off" name="nama" pattern="[a-z A-Z 0-9]+" autofocus="" required />
        </div>
        <div class="login__row">
          <svg class="login__icon pass svg-icon" viewBox="0 0 20 20">
            <path d="M0,20 20,20 20,8 0,8z M10,13 10,16z M4,8 a6,8 0 0,1 12,0" />
          </svg>
          <input type="password" class="login__input pass" placeholder="NIS / NIP"  
          maxlength="20" autocomplete="off" id="exampleInputEmail1" pattern="[a-z A-Z 0-9]+" name="nis" 
          required/>
        </div>
        <div>
		<select class="login__input name" name="level" id="hak__akses" >
	          <option selected="selected">PILIH AKSES</option>
	          <option value="siswa" style="color: black">SISWA</option>
	          <option value="guru" style="color: black">GURU</option>
	    </select>
        </div>
        <button type="submit" name="submit" class="login__zzz"style="margin-top: 10px !important;" >Login</button>

    	</form>
        <p class="login__signup2" style="margin-top: -10px !important;">Masuk sebagai petugas? &nbsp;<a href="login.php">Klik disini</a></p>
        <p class="login__signup" style="margin-top: -10px !important;"><a href="forgot_pas2.php">LUPA PASSWORD</a></p>
        <p class="login__signup" style="margin-top: -10px !important;"><a href="help2.php">HELP</a></p>

      </div>
    </div>
  </div>
</div>




	<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  	<script  src="assets/js/index.js"></script>

<?php 
include "footer.php";
?>


	

<?php
if(isset($_POST['submit'])){  
  include "koneksi.php";

  $u  = $_POST['nama'];
  $p  = $_POST['nis'];
  $level  = $_POST['level'];

  if ($level=='siswa') {
    $query =$koneksi->query("SELECT * FROM siswa WHERE nama_siswa='$u' AND nis='$p' ");

    $anjay=$query->num_rows;
    if($anjay == 0){
      echo '<div class="alert alert-danger">Upss...!!! Login gagal.</div>';
    }else{
      session_start();
      $row = $query->fetch_assoc();
      $_SESSION['id_pengguna']=$row['id_siswa'];
      $_SESSION['nama_level']='siswa';
      $_SESSION['identified']='peminjam';
      $_SESSION['nama_pengguna']=$row['nama_siswa'];
      header("Location:!peminjam/index.php");
    }

  }elseif ($level=='guru') {
    $query =$koneksi->query("SELECT * FROM guru WHERE nama_guru='$u' AND nip='$p' ");

    $anjay=$query->num_rows;
    if($anjay == 0){
      echo '<div class="alert alert-danger">Upss...!!! Login gagal.</div>';
    }else{
      session_start();
      $row = $query->fetch_assoc();
      $_SESSION['id_pengguna']=$row['id_guru'];
      $_SESSION['nama_level']='guru';
      $_SESSION['identified']='peminjam';
      $_SESSION['nama_pengguna']=$row['nama_guru'];
      header("Location:!peminjam/index.php");
    }
  }
}
?>