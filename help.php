<!DOCTYPE html>
<html>
<head>
	<title>HELP PAGE | MANUAL PROGRAN</title>

	<meta charset="utf-8">
	<meta name="keywords" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- CSS Bootstrap -->
	<link rel="stylesheet" type="text/css" href="assets/bt4/css/bootstrap.min.css">

	<script type="text/javascript" src="assets/bt4/js/jquery.js"></script>
	<script type="text/javascript" src="assets/bt4/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/bt4/js/jquery-1.11.1.min.js"></script>
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
  <a class="navbar-brand" href="#">S-I</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="#"><b>USER</b></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#menu"><b>MENU</b></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#peminjaman"><b>PEMINJAMAN</b></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#pengembalian"><b>PENGEMBALIAN</b></a>
      </li>      
    </ul>
  </div>
</nav>

	<div class="container">
        <p style="margin-top: 80px;">
        <a href="index.php" style="color: black"><b> --> KEMBALI KE FORM LOGIN <-- </b></a>
		</p>
		<div class="table-responsive">
			<table class="table table-striped table-hover table-bordered" style="margin-bottom: 0px;margin-top: 0px">
				<tr>
					<th colspan="4"><p align="center" style="margin-bottom: 0px;">LOGIN HELP</p></th>
				</tr>
				<tr>
					<th>Level</th>
					<th>Username</th>
					<th>Password</th>
				</tr>
				<tr>
					<th>Admin</th>
					<td>admin</td>
					<td>admin1</td>
				</tr>	
				<tr>
					<th>Operator</th>
					<td>oper</td>
					<td>oper</td>
				</tr>	
				<tr>
					<th>Siswa</th>
					<td>Maudhio Andre</td>
					<td>161707</td>
				</tr>	
				<tr>
					<th>Guru</th>
					<td>Abdul</td>
					<td>231</td>
				</tr>
			</table> 
		</div>
	<br>
	<br>
	<div id="menu">
		<p style="font-size: 20px;"><b>PENJELASAN MENU</b></p>
		<div class="row">
			<div class="col-md-3" style="margin-bottom: 20px;">
				<img src="img/help5.jpg">
			</div>
			<div class="col-md-9" style="font-size: 17px;">
				<b>Beranda</b> sebagai halaman awal. Berisi informasi singkat seperti grafik, jumlah barang, dll.<br><br>
				<b>Pengguna</b> berisi data admin, operator, siswa, dan guru yang dapat ditambah,ubah,dan hapus datanya.<br><br>
				<b>Data Ruang</b> menampilkan tempat dimana barang/inventaris berada. <br><br>
				<b>Data Jenis</b> menampilkan jenis-jenis barang/inventaris . <br><br>
				<b>Barang</b> Berisi data-data barang/inventaris. <br><br>
				<b>Pinjam Barang</b> adalah Menu untuk meminjam barang.<br><br> 
				<b>Pengembalian</b> Untuk mengembalikan barang dan melihat data pengembalian.<br><br>
				<b>Log</b> Berisi riwayat aktivitas yang telah dilakukan pengguna meliputi ubah data barang, hapus, dll.<br>

			</div>
		</div>
	</div>
	<br>
	<br>
	<div id="peminjaman">
	<p style="font-size: 20px;"><b>PANDUAN PEMINJAMAN</b></p>
	<p>ketika klik menu "Pinjam Barang" kalian akan disajikan barang-barang yang bisa dipinjam dalam bentuk kartu/Card.</p>
	<p><img src="img/help1.jpg" width="70%"></p>
	<p>Setelah kalian memilih barang mana yang ingin dipinjam, akan muncul pop-up form untuk mengkonfirmasi peminjaman,jika sudah yakin kalian bisa klik pinjam.</p>
	<img src="img/help2.jpg" width="40%" >
	<p>( *dalam 1 kali transaksi peminjaman, peminjam hanya dapat meminjam 1 jenis barang. )</p>
	</div>	
	<br>
	<br>
	<div id="pengembalian">
	<p style="font-size: 20px;"><b>PANDUAN PENGEMBALIAN</b></p>
	<p>ketika klik menu "Pengembalian" kalian akan disajikan Tabel yang berisi data peminjaman yang telah dikelompokkan berdasarkan nama peminjam. Klik 'Lengkap' untuk lihat data lengkap, atau klik 'lihat barang' untuk lihat data peminjaman berdasarkan nama peminjam. 'Pengembalian Lengkap' akan menampilkan barang yang telah dikembalikan.</p>
	<p><img src="img/help3.jpg" width="70%"></p>
	<p>Tabel dibawah ini akan muncul ketika klik 'lihat barang'. klik 'KEMBALIKAN' untuk mengembalikan barang yang dipinjam. 'Lap. Pengembalian' akan menampilkan barang yang telah dikembalikan oleh peminjam tersebut</p>
	<img src="img/help4.jpg" width="70%" >
	</div>
	

	</div>


</body>
</html>

