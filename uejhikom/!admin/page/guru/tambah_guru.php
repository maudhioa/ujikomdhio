<p class="title-content"><b>Tambah Data Guru</b></p>
<div id="hor-line"></div>

<form class="form-horizontal" method="POST" action="page/guru/proses.php?aksi=tambah_guru" enctype="multipart/form-data"> 
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label>Nama Guru * :</label>
				<input type="text" name="nama" class="form-control" autocomplete="off" placeholder="Nama Lengkap" required>
			</div>
			<div class="form-group">
				<label>NIP * :</label>
				<input type="text" name="nip" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Email * :</label>
				<input type="text" name="email" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Alamat</label>
				<textarea name="alamat" class="form-control"></textarea>
			</div>
		</div>
	</div><br>
	<button type="submit" class="btn btn-success">Tambah</button>
	<button type="button" class="btn btn-danger"><a href="?page=siswa">kembali</a></button>
</form>