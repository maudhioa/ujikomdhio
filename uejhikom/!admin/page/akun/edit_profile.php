<?php 
session_start();
include "koneksi.php";
$sql = $koneksi->query("SELECT * FROM petugas p LEFT JOIN level l ON p.id_lvl=l.id_level 
  WHERE id_petugas = $_SESSION[id_pengguna]");
$tampil = $sql->fetch_assoc();
?>


<p class="title-content"><b>Profile Pengguna/Akun</b></p>
<div id="hor-line"></div>

<a href="page/akun/export_db.php" class="btn btn-info" style="margin-bottom: 20px;">Export Database</a>
<style type="text/css">
	#td__profile{
		background-color: #f8f9fa;
		width: 30%;
	}
</style>
<div style="margin-bottom:15px">
    <p style="margin-bottom:5px">Pilih Bahasa :</p><br>
    <a href="../" class="btn btn-danger">INDONESIA</a>
    <a href="!adminuk/" class="btn btn-primary">INGGRIS</a>
</div>

<div class="table-responsive" style="width: 70%;">
<table class="table table-bordered table-hover">
		<tr>
			<td id="td__profile">Nama Pengguna</td>
			<td><?php echo $tampil['nama_petugas']; ?></td>
		</tr>
		<tr>
			<td id="td__profile">Level</td>
			<td><?php echo $tampil['nama_level']; ?></td>
		</tr>
		<tr>
			<td id="td__profile">Username</td>
			<td><?php echo $tampil['username']; ?></td>
		</tr>
		<tr>
			<td id="td__profile">Password</td>
			<td><?php echo $tampil['password']; ?></td>
		</tr>		
		<tr>
			<td colspan="2"><a href="" data-toggle="modal" data-target="#EditModal" class="btn btn-info">Edit</a></td>
		</tr>
</table>
</div>

<div>
  <div>Icons made by
   <a href="https://www.flaticon.com/authors/dave-gandy" title="Dave Gandy">Dave Gandy</a>
   <a href="https://www.freepik.com/" title="Freepik">Freepik</a>, 
   <a href="https://www.flaticon.com/authors/designmodo" title="Designmodo">Designmodo</a>, 
   <a href="https://www.flaticon.com/authors/itim2101" title="itim2101">itim2101</a>, 
   <a href="https://www.flaticon.com/authors/gregor-cresnar" title="Gregor Cresnar">Gregor Cresnar</a>, 
   <a href="https://www.flaticon.com/authors/kiranshastry" title="Kiranshastry">Kiranshastry</a>, 
   <a href="https://www.flaticon.com/authors/simpleicon" title="SimpleIcon">SimpleIcon</a><br> 

   from <a href="https://www.flaticon.com/"           title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/"          title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
 </p>
</div>


<div class="modal fade" id="EditModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="smallModalLabel" style="color: #222;">Edit Profile</h4>
      </div>
      <form class="form-horizontal" method="POST" action="page/akun/proses.php?aksi=edit_profile">
        <div class="modal-body" style="color: #222;">
          <div class="form-group">
            <label>Nama Petugas</label>
            <input type="text" name="nama_petugas" value="<?php echo $tampil['nama_petugas']; ?>" class="form-control" autocomplete="off">
            <input type="hidden" name="id_petugas" value="<?php echo $tampil['id_petugas']; ?>">
          </div>
          <div class="form-group">
            <label>Level</label>
            <select class="form-control" name="id_lvl">
              <?php
              include "koneksi.php";
              $query=$koneksi->query("SELECT * FROM level");
              while($sql=$query->fetch_assoc()) {
                ?>
                <option <?php if($tampil["id_lvl"]==$sql["id_level"]) echo "selected"; ?> value=<?php echo $sql["id_level"] ?>> <?php echo $sql["nama_level"] ?></option>                 
                <?php ;
              }               
              ?>
            </select>
          </div>
          <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" value="<?php echo $tampil['username']; ?>" class="form-control" autocomplete="off">
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" name="password" value="<?php echo $tampil['password']; ?>" class="form-control" autocomplete="off">
          </div>  
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Ubah</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>