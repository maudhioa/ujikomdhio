<p class="title-content"><b>Log / Aktivitas Pengguna</b></p>
<div id="hor-line"></div>


<nav class="nav nav-tabs" id="myTab" role="tablist" style="margin-top: 20px; font-size: 16px; margin-bottom: 15px;">
  <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#ubah" role="tab" aria-controls="nav-home" aria-selected="true">Ubah</a>
  <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#hapus" role="tab" aria-controls="nav-profile" aria-selected="false">Hapus</a>
</nav>


<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="ubah" role="tabpanel" aria-labelledby="nav-home-tab">
  	<div class="table-responsive">
			<table class="table table-bordered table-hover" id="example">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama</th>
						<th>Level</th>
						<th>Kegiatan</th>
						<th>id_inventaris</th>
						<th>Nama barang</th>
						<th>Jumlah</th>
						<th>Waktu</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no=1;
					include "koneksi.php"; 
					$query=$koneksi->query("SELECT * FROM log WHERE kegiatan='TELAH MENGUBAH' ");
					while ($data=$query->fetch_assoc()) {
						?>
						<tr>
							<td><?php echo $no ?></td>	
							<td><?php echo $data['nama_pengguna'] ?></td>	
							<td><?php echo $data['level'] ?></td>	
							<td><?php echo $data['kegiatan'] ?></td>		
							<td><?php echo $data['id_inventaris'] ?></td>		
							<td><?php echo $data['nama_barang'] ?></td>		
							<td><?php echo $data['jumlah'] ?></td>		
							<td><?php echo $data['waktu'] ?></td>		
							<td>
								<a onclick="return confirm('Apakah anda yakin???')" 
								href="page/log/proses.php?aksi=hapus&id_log=<?php echo $data['id_log'] ?>" class="btn btn-danger"><img src='../img/delete.png' width='16px'></a>
							</td>	
						</tr>
						<?php
						$no++;
					}
					?>
				</tbody>
			</table>
		</div>
  </div>

  <div class="tab-pane fade" id="hapus" role="tabpanel" aria-labelledby="nav-profile-tab">
  	<!--<p align="right" style="margin-bottom: 10px;">Buat Laporan :  <a href="page/pengembalian/lap_excel.php" class="btn btn-dark">Ex</a>
	<a href="page/pengembalian/lap_pdf.php" class="btn btn-dark">Pdf</a></p>-->
  	<div class="table-responsive">
			<table class="table table-bordered table-hover" id="example">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama</th>
						<th>Level</th>
						<th>Kegiatan</th>
						<th>id_inventaris</th>
						<th>Nama barang</th>
						<th>Jumlah</th>
						<th>Waktu</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no=1;
					include "koneksi.php"; 
					$query=$koneksi->query("SELECT * FROM log WHERE kegiatan='TELAH MENGHAPUS' ");
					while ($data=$query->fetch_assoc()) {
						?>
						<tr>
							<td><?php echo $no ?></td>	
							<td><?php echo $data['nama_pengguna'] ?></td>	
							<td><?php echo $data['level'] ?></td>	
							<td><?php echo $data['kegiatan'] ?></td>		
							<td><?php echo $data['id_inventaris'] ?></td>		
							<td><?php echo $data['nama_barang'] ?></td>		
							<td><?php echo $data['jumlah'] ?></td>		
							<td><?php echo $data['waktu'] ?></td>		
							<td>
								<a onclick="return confirm('Apakah anda yakin???')" 
								href="page/log/proses.php?aksi=hapus&id_log=<?php echo $data['id_log'] ?>" class="btn btn-danger"><img src='../img/delete.png' width='16px'></a>
							</td>	
						</tr>
						<?php
						$no++;
					}
					?>
				</tbody>
			</table>
		</div>
  </div>
</div>








<div class="tab-content" id="nav-tabContent">
	<div class="tab-pane fade show active" id="hapus" role="tabpanel" aria-labelledby="nav-home-tab">
		<div class="table-responsive">
			<table class="table table-bordered table-hover" id="example">
				
			</table>
		</div>
	</div>
</div>