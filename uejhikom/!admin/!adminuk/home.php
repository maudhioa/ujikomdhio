<?php 
class database{
	var $host = "localhost";
	var $uname = "u616110258_dhio";
	var $pass = "dhio325";
	var $db = "u616110258_dhio";
	var $koneksi;
	function __construct(){
		$this->koneksi = mysqli_connect($this->host, $this->uname, $this->pass,$this->db);
	}

	function tampil_barang(){
		$data = mysqli_query($this->koneksi,"SELECT COUNT(id_peminjaman) AS pinjam FROM peminjaman WHERE status_peminjaman='SEDANG DIPINJAM' ");
		$d = mysqli_fetch_array($data);
		$hasil = $d;
		return $hasil;
	}

	function tampil_barang2(){
		$data = mysqli_query($this->koneksi,"SELECT SUM(jumlah) AS barang FROM inventaris ");
		$d = mysqli_fetch_array($data);
		$hasil = $d;
		return $hasil;
	}

	function tampil_ruang(){
		$data = mysqli_query($this->koneksi,"SELECT count(id_ruang) AS ruang FROM ruang ");
		$d = mysqli_fetch_array($data);
		$hasil = $d;
		return $hasil;
	}	
}
 ?>

<script src="assets/Chart.js/Chart.bundle.js"></script>

<p class="title-content"><b>HOME</b></p>
<div id="hor-line"></div>
<?php 
include "../koneksi.php";
$db= new database();
?>
    <div class="row" style="margin-left: 0px; margin-right: 0px;">
    <?php
    $a=$db->tampil_barang(); 
    ?>
    <div class="col-md-4" style="background-color: grey; padding-top: 10px; padding-bottom: 10px; color:white;">
        <p align="center" style="color: white;"><?php echo $a['pinjam']; ?> items<br> are being borrowed</p>
    </div>

    <?php
    $a=$db->tampil_barang2(); 
    ?>
    <div class="col-md-4" style="background-color: darkgrey; padding-top: 10px; padding-bottom: 10px; color:red;">
        <p align="center" style="color: white;"><?php echo $a['barang']; ?> Total<br> Inventories</p>
    </div>
    
    <?php
    $a=$db->tampil_ruang(); 
    ?>
    <div class="col-md-4" style="background-color: black; padding-top: 10px; padding-bottom: 10px; color:red;">
        <p align="center" style="color: white;"><?php echo $a['ruang']; ?> Total<br> Rooms</p>
    </div>
    </div>

<div class="row" style="margin-top: 30px;">    
<div class="col-md-5" >
    <canvas id="myChart" width="100" height="70"></canvas>
</div>

<div class="col-md-5 offset-1" >
    <canvas id="myChart2" width="100" height="70"></canvas>
</div>
</div>
<!-- LIST CHARTS
line, bar, radar, doughnut, pie, polar area, bubble, scatter, area, mixed -->

<?php 
$query=$koneksi->query("SELECT jumlah AS ju FROM inventaris");
$query2=$koneksi->query("SELECT * FROM inventaris");
?>


<script>
            var ctx = document.getElementById("myChart");
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels:[<?php while ($b = $query2->fetch_array()) { echo '"' . $b['nama'] . '",';}?>],
                    datasets: [{
                            label:'Daftar List Jumlah Barang',
                            data: [<?php while ($c = $query->fetch_array()) { echo "$c[ju],";}?>],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                },
                options: {
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
</script>


<script>
            var ctx = document.getElementById("myChart2");
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels:['SEDANG DIPINJAM','TELAH DIKEMBALIKAN'],
                    datasets: [{
                            label:'Daftar List Jumlah Barang',
                            data: ['23','45'],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                },
                options: {
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
</script>