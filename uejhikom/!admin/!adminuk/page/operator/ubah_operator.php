<?php 
include "koneksi.php";
$id = $_GET['id_petugas'];
$query=$koneksi->query("SELECT * FROM petugas p JOIN level l ON p.id_lvl=l.id_level 
WHERE id_petugas=$id");
$data=$query->fetch_assoc();

 ?><form class="form-horizontal" method="POST" action="page/operator/proses.php?aksi=ubah_petugas">
          <div class="form-group">
            <label>Nama Petugas</label>
            <input type="text" value="<?php echo $data['nama_petugas'] ?>" name="nama_petugas" class="form-control" autocomplete="off">
            <input type="hidden" value="<?php echo $data['id_petugas'] ?>" name="id_petugas">
          </div>
          <div class="form-group">
            <label>Level</label>
            <select class="form-control" name="id_lvl">
                <option <?php if($data['id_lvl']=='1')echo "selected";?> value='1'>admin</option>
                <option <?php if($data['id_lvl']=='2')echo "selected";?> value='2'>operator</option>
            </select>
          </div>
          <div class="form-group">
            <label>Username</label>
            <input type="text" value="<?php echo $data['username'] ?>" name="username" class="form-control" autocomplete="off">
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="text" value="<?php echo $data['password'] ?>" name="password" class="form-control" autocomplete="off">
          </div>  
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Tambah</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </form>