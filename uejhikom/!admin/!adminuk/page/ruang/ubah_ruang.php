<?php 
include "../koneksi.php";
$id=$_GET['id'];
$query=$koneksi->query("SELECT * FROM ruang WHERE id_ruang='$id' ");
$data=$query->fetch_assoc();
?>

<p class="title-content"><b>Ubah Data Ruang</b></p>
<div id="hor-line"></div>

<form class="form-horizontal" method="POST" action="page/ruang/proses.php?aksi=ubah" enctype="multipart/form-data"> 
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label>Nama Ruang * :</label>
				<input type="text" name="nama" value="<?php echo $data['nama_ruang']; ?>" class="form-control" autocomplete="off" required>
				<input type="hidden" name="id_ruang" value="<?php echo $data['id_ruang']; ?>" required>
			</div>
			<div class="form-group">
				<label>Kode Ruang * :</label>
				<input type="text" name="kode" value="<?php echo $data['kode_ruang']; ?>" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Keterangan</label>
				<textarea name="keterangan" class="form-control"><?php echo $data['keterangan']; ?></textarea>
			</div>
		</div>
	</div><br>
	<button type="submit" class="btn btn-success">Ubah</button>
	<button type="button" class="btn btn-danger"><a href="?page=ruang">kembali</a></button>
</form>