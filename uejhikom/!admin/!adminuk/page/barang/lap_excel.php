<?php 
include "database.php";
$data = new database();
$tampil = $data->tampil_barang(); 
?>


<p align="center" id="title-laporan">DATA BARANG</p><br>
<a type="button" value="EXCEL" href="" id="cetak" class="no-print"> </a>

<table border="1" width="100%" style="border-collapse: collapse;">
	<thead class="title-table">
		<tr style="height: 40px;">
			<th>No</th>
			<th>Nama</th>
			<th>Jumlah</th>
			<th>Kondisi</th>
			<th>Nama Jenis</th>
			<th>Nama ruang</th>
			<th>Tanggal Register</th>
			<th>Nama_petugas</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$no=1;
		foreach ($tampil as $a) {
		?>
		<tr style="height: 30px; text-align: center;">
			<td><?php echo $no; ?></td>
			<td><?php echo $a["nama"]; ?></td>
			<td><?php echo $a["jumlah"]; ?></td>
			<td><?php echo $a["kondisi"]; ?></td>
			<td><?php echo $a["nama_jenis"]; ?></td>
			<td><?php echo $a["nama_ruang"]; ?></td>
			<td><?php echo $a["tanggal_register"]; ?></td>
			<td><?php echo $a["nama_petugas"]; ?></td>
		</tr>
		<?php 	
		 $no++;
		 } 
		 ?>
	</tbody>
</table>




<?php

header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=data-barang.xls");
 
// Tambahkan table
?>