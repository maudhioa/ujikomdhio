<?php 
include "database.php";
$tampil = new database();
?>
<p class="title-content"><b>Pengembalian Barang</b></p>
<div id="hor-line"></div>
<a class="btn btn-dark" href="?page=pengembalian" style="margin-bottom: 10px;">Kembali</a>


<div class="table-responsive">
			<table class="table table-bordered table-hover" id="example">
				<thead>
					<tr>
						<th>No</th>
						<th>Kode</th>
						<th>Nama</th>
						<th>Level</th>
						<th>Barang</th>
						<th>Jumlah Pinjam</th>
						<th>Tanggal Pinjam</th>
						<th>Status</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no=1;
					$a = $tampil->pengembalian_lengkap1("TELAH DIKEMBALIKAN"); 
					foreach ($a as $data) {
						?>
						<tr>
							<td><?php echo $no ?></td>	
							<td><?php echo $data['kode_peminjaman'] ?></td>	
							<td><?php echo $data['nama_peminjam'] ?></td>	
							<td><?php echo $data['level'] ?></td>	
							<td><?php echo $data['nama'] ?></td>	
							<td><b><?php echo $data['jumlah_pinjam'] ?></b> Buah/Unit</td>	
							<td><?php echo $data['tanggal_pinjam'] ?></td>	
							<td><?php echo $data['status_peminjaman'] ?></td>	
							<td>
								<?php echo "
								<a class='btn btn-danger' style='color:white;font-size:13px' href='page/pengembalian/proses.php?aksi=kembalikan2&kode_peminjaman=$data[kode_peminjaman]&id_peminjam=$data[id_peminjam]'>KEMBALIKAN</a> " 
								?>
							</td>	
						</tr>
						<?php
						$no++;
					}
					?>
				</tbody>
			</table>
		</div>
  </div>
