

<p class="title-content"><b>Profile Pengguna/Akun</b></p>
<div id="hor-line"></div>

<style type="text/css">
	#td__profile{
		background-color: #f8f9fa;
		width: 30%;
	}
</style>


<?php
session_start(); 
include "koneksi.php";
if ($_SESSION["nama_level"]=='siswa') {
  $sql = $koneksi->query("SELECT * FROM siswa WHERE id_siswa=$_SESSION[id_pengguna]");
  $tampil = $sql->fetch_assoc();  
?>
  <div class="table-responsive" style="width: 70%;">
<table class="table table-bordered table-hover">
    <tr>
      <td id="td__profile">Nama Siswa</td>
      <td><?php echo $tampil['nama_siswa']; ?></td>
    </tr>
    <tr>
      <td id="td__profile">NIS</td>
      <td><?php echo $tampil['nis']; ?></td>
    </tr>
    <tr>
      <td id="td__profile">Alamat</td>
      <td><?php echo $tampil['alamat']; ?></td>
    </tr>   
    <tr>
      <td colspan="2"><a href="" data-toggle="modal" data-target="#EditModal" class="btn btn-info">Edit</a></td>
    </tr>
</table>
</div>

<div class="modal fade" id="EditModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="smallModalLabel" style="color: #222;">Edit Profile</h4>
      </div>
      <form class="form-horizontal" method="POST" action="">
        <div class="modal-body" style="color: #222;">
          <div class="form-group">
            <label>Nama Siswa</label>
            <input type="text" name="nama_siswa" value="<?php echo $tampil['nama_siswa']; ?>" class="form-control" autocomplete="off">
            <input type="hidden" name="id_siswa" value="<?php echo $_SESSION['id_pengguna']; ?>">
          </div>
          <div class="form-group">
            <label>NIS</label>
            <input type="text" name="nis" value="<?php echo $tampil['nis']; ?>" class="form-control" autocomplete="off">
          </div>
          <div class="form-group">
            <label>Alamat</label>
            <textarea class="form-control" name="alamat"><?php echo $tampil['alamat']; ?></textarea>
          </div>  
        </div>
        <div class="modal-footer">
          <button type="submit" name="ubah" class="btn btn-success">Ubah</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php
if(isset($_POST['ubah'])) {
  $id_siswa=$_POST['id_siswa']; 
$nama_siswa=$_POST['nama_siswa']; 
$nis=$_POST['nis']; 
$alamat=$_POST['alamat']; 
$query=$koneksi->query("UPDATE siswa SET nama_siswa='$nama_siswa',nis='$nis',alamat='$alamat' WHERE id_siswa='$id_siswa' ");
  if ($query) {
  header("location:index.php?page=edit_profile");
  }
}


}elseif ($_SESSION["nama_level"]=='guru') {
  $sql = $koneksi->query("SELECT * FROM guru WHERE id_guru=$_SESSION[id_pengguna]");
  $tampil = $sql->fetch_assoc();
?>
  <div class="table-responsive" style="width: 70%;">
<table class="table table-bordered table-hover">
    <tr>
      <td id="td__profile">Nama Guru</td>
      <td><?php echo $tampil['nama_guru']; ?></td>
    </tr>
    <tr>
      <td id="td__profile">NIP</td>
      <td><?php echo $tampil['nip']; ?></td>
    </tr>
    <tr>
      <td id="td__profile">Alamat</td>
      <td><?php echo $tampil['alamat']; ?></td>
    </tr>   
    <tr>
      <td colspan="2"><a href="" data-toggle="modal" data-target="#EditModal" class="btn btn-info">Edit</a></td>
    </tr>
</table>
</div>


<div class="modal fade" id="EditModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="smallModalLabel" style="color: #222;">Edit Profile</h4>
      </div>
      <form class="form-horizontal" method="POST" action="">
        <div class="modal-body" style="color: #222;">
          <div class="form-group">
            <label>Nama Guru</label>
            <input type="text" name="nama_guru" value="<?php echo $tampil['nama_guru']; ?>" class="form-control" autocomplete="off">
            <input type="hidden" name="id_guru" value="<?php echo $_SESSION['id_pengguna']; ?>">
          </div>
          <div class="form-group">
            <label>NIP</label>
            <input type="text" name="nip" value="<?php echo $tampil['nip']; ?>" class="form-control" autocomplete="off">
          </div>
          <div class="form-group">
            <label>Alamat</label>
            <textarea class="form-control" name="alamat"><?php echo $tampil['alamat']; ?></textarea>
          </div>  
        </div>
        <div class="modal-footer">
          <button type="submit" name="ubah" class="btn btn-success">Ubah</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php
if(isset($_POST['ubah'])) {
  $id_guru=$_POST['id_guru']; 
$nama_guru=$_POST['nama_guru']; 
$nip=$_POST['nip']; 
$alamat=$_POST['alamat']; 
$query=$koneksi->query("UPDATE guru SET nama_guru='$nama_guru',nip='$nip',alamat='$alamat' WHERE id_guru='$id_guru' ");
  if ($query) {
  header("location:index.php?page=edit_profile");
  }
}
?>

<?php  
}
?>



