<?php 
include "function.php";
$data = new database();
session_start(); 
?>

<link rel="stylesheet" type="text/css" href="../../assets/css/style.css">

<p align="center" id="title-laporan">DATA BARANG</p><br>
<input type="button" value="Buat PDF" onclick="window.print()" id="cetak" class="no-print"> 

<table border="1" width="100%" style="border-collapse: collapse;">
	<thead class="title-table">
		<tr style="height: 40px;">
			<th>No</th>
			<th>Nama Peminjam</th>
			<th>Level</th>
			<th>Barang</th>
			<th>Jumlah</th>
			<th>Tgl. Pinjam</th>
			<th>Tgl. Kembali</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$no=1;
		$tampil = $data->tampil_pengembalian2("TELAH DIKEMBALIKAN",$_SESSION['id_pengguna']); 
		foreach ($tampil as $a) {
		?>
		<tr style="height: 30px; text-align: center;">
			<td><?php echo $no; ?></td>
			<td><?php echo $_SESSION["nama_pengguna"]; ?></td>
			<td><?php echo $_SESSION["nama_level"]; ?></td>
			<td><?php echo $a["nama"]; ?></td>
			<td><?php echo $a["jumlah"]; ?></td>
			<td><?php echo $a["tanggal_pinjam"]; ?></td>
			<td><?php echo $a["tanggal_kembali"]; ?></td>
		</tr>
		<?php 	
		 $no++;
		 } 
		 ?>
	</tbody>
</table>
