<p class="title-content"><b>Pinjam Barang / Inventaris</b></p>
<div id="hor-line"></div>


<?php 
include "../koneksi.php";
include "kodepj.php";
session_start();
$modal=$koneksi->query("SELECT * FROM inventaris");
while ($sq = $modal->fetch_assoc()) 
{
  ?>
  <div class="modal fade" id="pinjam<?=$sq['id_inventaris'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="max-width: 700px;">
      <div class="modal-content">
        <div class="modal-header">
          <h3><b>Data Lengkap Inventaris/Barang</b></h3><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
      <form class="form-horizontal" method="POST" action="page/pinjam/proses.php?aksi=pinjam">
        <div class="modal-body">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead></thead>
              <tbody>
                <?php
                $dataa=$koneksi->query("SELECT * FROM inventaris i JOIN ruang r ON i.id_ruang=r.id_ruang JOIN jenis j ON i.id_jenis=j.id_jenis JOIN petugas p ON i.id_petugas=p.id_petugas WHERE id_inventaris=$sq[id_inventaris]" );
                $no=1;
                while ($ra=$dataa->fetch_assoc())
                { 

                  echo"<tr>";
                  echo '<th>Kode Peminjaman</th>';
                  echo "<td><input type='text' name='kode_peminjaman' value='$kode' class='form-control' readonly>
                  </td>";
                  echo"</tr>";

                  echo"<tr>";
                  echo '<th>Nama Peminjam</th>';
                  echo "<td><input type='text' value='$_SESSION[nama_pengguna]' class='form-control' readonly><input type='hidden' name='id_peminjam' value='$_SESSION[id_pengguna]'>
                  <input type='hidden' name='nama_pengguna' value='$_SESSION[nama_pengguna]'>
                  <input type='hidden' name='nama_level' value='$_SESSION[nama_level]'>
                  </td>";
                  echo"</tr>";

                  echo"<tr>";
                  echo '<th>Barang</th>';
                  echo "<td><input type='text' value='$ra[nama]' class='form-control' readonly><input type='hidden' name='id_inventaris' value='$ra[id_inventaris]'>
                  </td>";
                  echo"</tr>";

                  echo"<tr>";
                  echo '<th>Kondisi</th>';
                  echo "<td><input type='text' value='$ra[kondisi]' class='form-control' readonly>
                  </td>";
                  echo"</tr>";                  

                  echo"<tr>";
                  echo '<th>Jumlah</th>';
                  echo "<td><input type='number' name='jumlah' value='1' class='form-control'>
                  </td>";
                  echo"</tr>";

                  echo"<tr>";
                  echo"<th colspan=5><button type='submit' class='btn btn-success'>Pinjam</button></th>";      
                  echo"</tr>";
                  $no++;
                }
                ?>
              </tbody>
            </table>
          </div>    
        </div>
      </form>
      </div>
    </div>
  </div>
<?php } ?>



   <nav class="nav nav-tabs" id="myTab" role="tablist">
      <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Semua</a>
      <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Elektronik</a>
      <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Olahraga</a>
      <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#pembelajaran" role="tab" aria-controls="nav-contact" aria-selected="false">Pembelajaran</a>

    </nav>
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="nav-home">
        <div class="row" style="width: 100%; margin-top: 20px;">
          <?php 
      //include "kodepj.php";
          include "../koneksi.php";

          $sql=$koneksi->query("SELECT * FROM inventaris i JOIN jenis j ON i.id_jenis=j.id_jenis WHERE nama_jenis!='Alat Tetap' ");
          while ($tampil=$sql->fetch_assoc()) {
            ?>
              <div class="col-md-3 col-6">
              <a data-toggle='modal' data-target='#pinjam<?php echo $tampil[id_inventaris] ?>' href=""> 
              <div class='card' style='width: 110%; margin-bottom: 20%;'>
                <img src='page/pinjam/img/<?php echo $tampil['foto'] ?>' width='100%' height='200px' alt='Card image cap'>
                <div class='card-body'>
                  <h4 class='card-title'> <?php echo $tampil["nama"] ?></h4>
                  <p class='card-text'>
                    Jumlah : <?php echo $tampil["jumlah"] ?><br>
                    Keadaan : <?php echo $tampil["kondisi"] ?><br>
                  </p>
                </div>
              </div>
              </a>
              </div>
            <?php 
            ;
          }
          ?>
        </div>
      </div>
      <div class="tab-pane fade" id="nav-profile" >
        <div class="row" style="width: 100%; margin-top: 20px;">
          <?php 
      //include "kodepj.php";
          include "../koneksi.php";

          $sql=$koneksi->query("SELECT * FROM inventaris i JOIN jenis j ON i.id_jenis=j.id_jenis WHERE nama_jenis='Alat Elektronik' ");
          while ($tampil=$sql->fetch_assoc()) {
            ?>
              <div class="col-md-3 col-6">
              <a data-toggle='modal' data-target='#pinjam<?php echo $tampil[id_inventaris] ?>' href=""> 
              <div class='card' style='width: 110%; margin-bottom: 20%;'>
                <img src='page/pinjam/img/<?php echo $tampil['foto'] ?>' width='100%' height='200px' alt='Card image cap'>
                <div class='card-body'>
                  <h4 class='card-title'> <?php echo $tampil["nama"] ?></h4>
                  <p class='card-text'>
                    Jumlah : <?php echo $tampil["jumlah"] ?><br>
                    Keadaan : <?php echo $tampil["kondisi"] ?><br>
                  </p>
                </div>
              </div>
              </a>
              </div>
            <?php 
            ;
          }
          ?>
        </div>
      </div>
      <div class="tab-pane fade" id="nav-contact" >
        <div class="row" style="width: 100%; margin-top: 20px;">
          <?php 
      //include "kodepj.php";
          include "../koneksi.php";

          $sql=$koneksi->query("SELECT * FROM inventaris i JOIN jenis j ON i.id_jenis=j.id_jenis WHERE nama_jenis='Alat Olahraga' ");
          while ($tampil=$sql->fetch_assoc()) {
            ?>
              <div class="col-md-3 col-6">
              <a data-toggle='modal' data-target='#pinjam<?php echo $tampil[id_inventaris] ?>' href=""> 
              <div class='card' style='width: 110%; margin-bottom: 20%;'>
                <img src='page/pinjam/img/<?php echo $tampil['foto'] ?>' width='100%' height='200px' alt='Card image cap'>
                <div class='card-body'>
                  <h4 class='card-title'> <?php echo $tampil["nama"] ?></h4>
                  <p class='card-text'>
                    Jumlah : <?php echo $tampil["jumlah"] ?><br>
                    Keadaan : <?php echo $tampil["kondisi"] ?><br>
                  </p>
                </div>
              </div>
              </a>
              </div>
            <?php 
            ;
          }
          ?>
        </div>
      </div>
      <div class="tab-pane fade" id="pembelajaran" >
        <div class="row" style="width: 100%; margin-top: 20px;">
          <?php 
      //include "kodepj.php";
          include "../koneksi.php";

          $sql=$koneksi->query("SELECT * FROM inventaris i JOIN jenis j ON i.id_jenis=j.id_jenis WHERE nama_jenis='Pembelajaran' ");
          while ($tampil=$sql->fetch_assoc()) {
            ?>
              <div class="col-md-3 col-6">
              <a data-toggle='modal' data-target='#pinjam<?php echo $tampil[id_inventaris] ?>' href=""> 
              <div class='card' style='width: 110%; margin-bottom: 20%;'>
                <img src='page/pinjam/img/<?php echo $tampil['foto'] ?>' width='100%' height='200px' alt='Card image cap'>
                <div class='card-body'>
                  <h4 class='card-title'> <?php echo $tampil["nama"] ?></h4>
                  <p class='card-text'>
                    Jumlah : <?php echo $tampil["jumlah"] ?><br>
                    Keadaan : <?php echo $tampil["kondisi"] ?><br>
                  </p>
                </div>
              </div>
              </a>
              </div>
            <?php 
            ;
          }
          ?>
        </div>
      </div>
    </div>


    
